pagure for Debian
=================

Configuring pagure
------------------

## Introduction

After you've installed the package, you will need to perform some
manual operations in order to get everything working.

You can find a quick guide of what needs to be configured below.  If
you need more help or information, you can refer to the official
documentation (available in the 'pagure-doc' package).

This document was based on the "pagure-README.Fedora" file, available
at:

  <https://src.fedoraproject.org/rpms/pagure/blob/master/f/pagure-README.Fedora>

1) Install and set up a database

Pagure comes configured to use SQLite by default, which should be OK
for testing purposes, but is probably not the best choice for
production servers.

If you would like to set up a MariaDB or a PostgreSQL database with
pagure, you will need to install the necessary packages
('mariadb-server' or 'postgresql', depending on your choice) and
create the database.  Note that you just need to install the database
server if you intend to run it on the same machine as pagure.  If you
already have a database server running at another location, you can
use it.

1.1) Using MariaDB

Execute the following commands as MariaDB admin:

  mysql> CREATE DATABASE pagure;
  mysql> GRANT ALL PRIVILEGES ON pagure.* TO _pagure IDENTIFIED BY '<PiCk A PaSsWoRd>';
  mysql> FLUSH PRIVILEGES;
  mysql> EXIT;

The system pagure user is called "_pagure", so it is a good practice
to use the same name as the database user.

Don't forget to replace <PiCk A PaSsWoRd> by the password you would
like to use.

1.2) Using PostgreSQL

Switch to the 'postgres' user and execute:

  $ createuser --pwprompt _pagure
  $ createdb -O _pagure pagure

The first command (createuser) will ask for a password to be used for
the database user.

You can test if everything worked by doing (as root, for example):

  # psql -d pagure -h localhost -U _pagure

1.3) Using SQLite

If you want to use SQLite, you just need to install the package (it is
called 'sqlite3').

2) Install Redis

Pagure needs Redis to work.  You can install it locally using the
'redis-server' package, or you can configure pagure to talk to a Redis
server running in another machine (see the /etc/pagure/pagure.cfg
configuration file).

3) Inform pagure about your database options

If you chose to use a database different than SQLite, you must edit
the file /etc/pagure/alembic.ini, and change the line:

  sqlalchemy.url = sqlite:////var/tmp/pagure_dev.sqlite

to:

  sqlalchemy.url = mysql+pymysql://_pagure:<PiCk A PaSsWoRd>@localhost/pagure

if you chose to use MariaDB with 'python3-pymysql', or to:

  sqlalchemy.url = postgres+pg8000://_pagure:<PiCk A PaSsWoRd>@localhost/pagure

if you chose to use PostgreSQL with 'python3-pg8000'.

You also need to edit the file /etc/pagure/pagure.cfg, comment out the
'DB_URL' parameter (which is set to use SQLite by default), and
uncomment the correct 'DB_URL' option (depending on the database you
chose to use).

5) Edit the configuration files

You will need to edit the file /etc/pagure/pagure.cfg and fill it with
information about your instance.

The Debian pagure package uses /var/lib/pagure/ as the directory which
will contain everything about the instance (like the git
repositories), except for the "releases" folder, which is located at
/var/www/releases.

You will also want to take a good look at this configuration file, and
change many things on it.  For example, changing the email address,
domain name, git configuration, and many other settings might be a
good idea, especially if you intend to make your instance public.

The file is reasonably well commented, but you might also want to look
at the official documentation, either by installing the 'pagure-doc'
package or by going directly to:

  <https://docs.pagure.org/pagure/configuration.html>

6) Enable the "pagure" git authentication backend

As of Pagure 5.8.1, the "gitolite3" git authentication backend does
not work with recent gitolite3 packages (see
<https://pagure.io/pagure/issue/3971>).  Debian carries a recent
gitolite3.  For this reason, Debian's pagure package comes
pre-configured to use the "pagure" git authentication backend, which
works just as well.

You have two options to enable the "pagure" git authentication
backend.

The first option, which comes pre-configured, is to use the new
'pagure-authorized-keys-worker' service, which is responsible for
writing the 'authorized_keys' file that will be used for
authentication.  This option does not require you to modify any system
files, but is slower, impossible to scale to multiple Pagure frontend
instances on top of a shared git repository without causing races, and
is not recommended for bigger instances.

The second option is to directly modify the /etc/ssh/sshd_config file
and instruct the SSH server to ask Pagure to validate a SSH user key.
This has the advantage of performance, but involves modifying sshd's
configuration file, which may not be feasible for everybody.

6.1) Using the 'pagure-authorized-keys-worker' worker

Pagure on Debian comes pre-configured to use the new
'pagure-authorized-keys-worker' worker.  All you have to do is enable
and start it:

  # systemctl enable --now pagure-authorized-keys-worker.service

6.2) Modifying the /etc/ssh/sshd_config file

Unfortunately, you will need to make a small modification to your
/etc/ssh/sshd_config file in order to make things work.  You will have
to add the following two lines to it:

  AuthorizedKeysCommand /usr/share/pagure/keyhelper.py "%u" "%h" "%t" "%f"
  AuthorizedKeysCommandUser _pagure

Make sure to reload your sshd after editing the file:

  # systemctl reload sshd.service

You will also have to edit /etc/pagure/pagure.cfg and modify:

  GIT_AUTH_BACKEND = "pagure"

7) Set up the database

After you've edited the configuration files, you can run:

  $ python3 /usr/share/pagure/createdb.py -c /etc/pagure/pagure.cfg -i /etc/pagure/alembic.ini

This will set up the database.

If you are upgrading pagure and already have an existing database, you
can omit the "-i ..." part of the command:

  $ python3 /usr/share/pagure/createdb.py -c /etc/pagure/pagure.cfg

8) Edit the Apache2 configuration file (or create one, if you're not
   using Apache2)

If you're using Apache2, you must edit the configuration file that is
shipped with pagure.  The file can be found at
/etc/apache2/conf-available/pagure.conf.  It comes with several
settings pre-configured; you will "just" have to edit things like the
hostname, VirtualHost, SSL/TLS support.

9) Open ports in the firewall, if needed

If you use a firewall, don't forget to open the necessary ports if
needed.  You will likely need to open:

- Port 80 (HTTP) or 443 (HTTPS), depending on your configuration
- Port 22 (SSH)

You may have to open other ports depending on your setup.

10) Finally, start pagure and its timers!

  # systemctl enable --now pagure-worker \
			   pagure-api-key-expire-mail.timer \
			   pagure-mirror-project-in.timer

If you are using TLS, you will need to enable the "ssl" and the
"headers" Apache modules:

  # a2enmod ssl headers

Enable the Apache configuration file, and restart Apache:

  # a2enconf pagure
  # systemctl restart apache2.service

If the configuration files are OK, the service should start without
problems.

11) Install and enable other pagure services (optional)

Debian ships quite a number of pagure packages which extend the
regular functionality of pagure.  A simple:

  # apt search pagure-

should be able to list the extra packages available for installation.
Please, keep in mind that these packages come with systemd service
files that are not enabled/started by default, so you will have to do
that yourself.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Fri, 19 Jul 2019 14:24:52 -0400
