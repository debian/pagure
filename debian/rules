#!/usr/bin/make -f

export PYBUILD_NAME = pagure

DESTDIR = debian/pagure
export PYBUILD_DESTDIR = $(DESTDIR)

DESTDIR_EV_SERVER = debian/pagure-ev-server

# The user we create when the package is installed.
PAGURE_USER = _pagure

%:
	dh $@ --with python3,sphinxdoc,apache2 --buildsystem=pybuild


override_dh_auto_clean:
	rm -rf doc/_build
	dh_auto_clean

override_dh_auto_build:
	dh_auto_build
	PYTHONPATH=$(CURDIR) http_proxy='127.0.0.1:9' make -C doc/ \
	    html SPHINXBUILD=sphinx-build
# For the manpage, we don't need to include the changelog.  This also
# prevents an annoying lintian warning.
	PYTHONPATH=$(CURDIR) http_proxy='127.0.0.1:9' make -C doc/ \
            man SPHINXBUILD=sphinx-build \
	    SPHINXOPTS='-D exclude_patterns=changelog.rst'

# Unfortunately, there are a lot of tweaks we need to do after
# auto_install.  For each Python 3 version available, we need to:
#
# 1) Iterate over the *.js files (but _not_ *.min.js), and minify it
# using yui-compressor.
#
# 2) Likewise, but for *.css files.
#
# 3) Iterate over the symlinks under pagure/hooks/files/, check if it
# points to a non-existent file, and remove it if it does.
#
# 4) Iterate over the font files and remove the executable bits from
# them.

# eventually this should be replaced by building from real source
# emojione/emojione.sprites.css is a link to the minified version, removed here because it is broken, put back in pagure.links
override_dh_auto_install:
	dh_auto_install
	mkdir -p $(DESTDIR)/usr/share/pagure
	for py in $(shell py3versions -rv); do \
	    paguredir=$(DESTDIR)/usr/lib/python$${py}/dist-packages/pagure; \
	    themesdir=$${paguredir}/themes; \
	    vendordir=$${paguredir}/static/vendor; \
	    rm -rfv $${vendordir}/jquery.atwho $${vendordir}/*/*.min.js $${vendordir}/*/*.min.css $${vendordir}/emojione/emojione.sprites.css ; \
	    find $${vendordir} $${themesdir} -name "*.js" ! -name "*.min.js" \
	      | sed 's/\.js$$//' | while read file ; do \
	        echo "Processing $${file}.js" && \
	        terser -c -o $${file}.min.js $${file}.js || cp $${file}.js $${file}.min.js; \
	    done; \
	    find $${vendordir} $${themesdir} -name "*.css" ! -name "*.min.css" \
	      | sed 's/\.css$$//' | while read file ; do \
	        echo "Processing $${file}.css" && \
	        yui-compressor -o $${file}.min.css $${file}.css || cp $${file}.css $${file}.min.css; \
	    done; \
	    find $${paguredir}/hooks/files/ -type l \
	      | while read file; do \
	        if stat $${file} | grep -q '\/does\/not\/exist'; then \
	            rm -fv $${file}; \
	        fi; \
	    done; cp -rfv $${paguredir}/static $${paguredir}/templates $${paguredir}/themes -t $(DESTDIR)/usr/share/pagure; \
	done

override_dh_install:
	dh_install
# We install the directories below under /usr/share/pagure, not python3*
# (from the copy we minified above, not the original that we omit minified files from)
	rm -rfv $(DESTDIR)/usr/lib/python*/dist-packages/pagure/static/
	rm -rfv $(DESTDIR)/usr/lib/python*/dist-packages/pagure/templates/
	rm -rfv $(DESTDIR)/usr/lib/python*/dist-packages/pagure/themes/
# clean up after tests
	rm -rfv $(DESTDIR)/usr/lib/python*/dist-packages/dump.rdb

# These are helper scripts, but they need to be marked executable.
override_dh_fixperms:
	dh_fixperms
	chmod -v +x \
	    $(DESTDIR)/usr/share/pagure/aclchecker.py \
	    $(DESTDIR)/usr/share/pagure/keyhelper.py \
	    $(DESTDIR)/usr/share/pagure/mirror_project_in.py \
	    $(DESTDIR_EV_SERVER)/usr/share/pagure-ev-server/bin/pagure_stream_server.py

override_dh_installsystemd-indep:
	dh_installsystemd --no-enable --no-start --package pagure --name pagure-worker
	dh_installsystemd --no-enable --no-start --package pagure --name pagure-gitolite-worker
	dh_installsystemd --no-enable --no-start --package pagure --name pagure-api-key-expire-mail
	dh_installsystemd --no-enable --no-start --package pagure --name pagure-mirror-project-in
	dh_installsystemd --no-enable --no-start --package pagure --name pagure-authorized-keys-worker
	dh_installsystemd --no-enable --no-start --remaining-packages
# The service files come with user/group "git" (or "mirror", in the
# case of pagure-mirror) by default.  We have to replace it with our
# own user/group $(PAGURE_USER).
	find debian/ -type f -name "*.service" \
	  | xargs sed -i -e "s/User=\(git\|mirror\)/User=$(PAGURE_USER)/" \
		      -e "s/Group=\(git\|mirror\)/Group=$(PAGURE_USER)/"

# The Apache2 configuration file that is installed must be edited and
# adjusted by the user before it can be enabled.
override_dh_apache2:
	dh_apache2 --noenable

# test_style is skipped as code style checks do not belong in distribution builds
# test_upload_issue_virus is skipped as it requires clamd
# others listed currently fail
TEST_SKIPS=not test_style and not test_upload_issue_virus and not test_dumping_reloading_ticket and not test_plugin and not test_delete_repo

# this is used if not set below i.e. not Salsa or autopkgtest
# full test suite in one run
# this may take several hours
PYBUILD_TEST_ARGS=-k '$(TEST_SKIPS)'

ifneq (,$(CI_COMMIT_MESSAGE))
# Salsa
# pick some at random-ish for a quick test
# intentionally different from autopkgtest for more coverage
PYBUILD_TEST_ARGS=-k 'commit and $(TEST_SKIPS)'
# previously failing ones
#PYBUILD_TEST_ARGS=-k 'test_install_plugin or test_dumping_reloading_ticket or test_plugin or test_force_logout or test_delete_repo or test_edit_file or new_project_with_template'
ifneq (,$(findstring full ci,$(CI_COMMIT_MESSAGE)))
# full run on Salsa
# use both architectures to fit in the 3hr limit
export PYBUILD_DISABLE=test/python3.12
ifeq ($(DEB_HOST_ARCH),i386)
PYBUILD_TEST_ARGS=-k '(pagure_lib or pagure_flask_api) and $(TEST_SKIPS)'
else
PYBUILD_TEST_ARGS=-k 'not pagure_lib and not pagure_flask_api and $(TEST_SKIPS)'
endif
endif
endif
ifneq (,$(PYBUILD_AUTOPKGTEST))
# autopkgtest: pick some at random-ish for a quick test, plus the known pygit2 1.17 fail
PYBUILD_TEST_ARGS=-k '(fork or new_project_with_template) and $(TEST_SKIPS)'
endif
export PYBUILD_TEST_ARGS

# Necessary for the tests.
export PAGURE_CONFIG = $(CURDIR)/tests/test_config

