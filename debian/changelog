pagure (5.14.1+dfsg-7) unstable; urgency=medium

  * Tests: ignore (in more places) that the default token expiration
    is from import time not from token creation time.

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Mon, 10 Feb 2025 23:18:12 +0000

pagure (5.14.1+dfsg-6) unstable; urgency=medium

  * Tests: allow some build path special characters in more places,
    allow no Location header,
    ignore that the default token expiration is from import time
    not from token creation time.

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Sun, 09 Feb 2025 14:09:07 +0000

pagure (5.14.1+dfsg-5) unstable; urgency=medium

  * Don't error with newer versions of dependencies.
    - On some plugin use, online file edits, and project creation.
  * Disable tests that still fail.
    - Plugins, force logout, repo deletion, dump and reload.
  * Tests: allow build paths containing some special characters,
    allow building in non-UTC time zones.

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Sat, 08 Feb 2025 19:44:44 +0000

pagure (5.14.1+dfsg-4) unstable; urgency=medium

  * Tests: accept merge messages with seconds,
    stop ignoring failures.

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Tue, 04 Feb 2025 07:25:25 +0000

pagure (5.14.1+dfsg-3) unstable; urgency=medium

  * Tests: ignore failures, add debug output.

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Tue, 28 Jan 2025 22:08:31 +0000

pagure (5.14.1+dfsg-2) unstable; urgency=medium

  * Fix Javascript error in security patch.
  * Fix bugs introduced by pygit2 compatibility patch.
  * Tests: Replace mock with unittest.mock (Closes: #1093648),
    skip fewer tests, accept more changed error messages.
  * d/copyright: fix Lintian warning.

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Mon, 27 Jan 2025 21:42:25 +0000

pagure (5.14.1+dfsg-1) unstable; urgency=medium

  * New upstream release.  Includes security fixes (Closes: #1091383):
    - Do not allow reading or writing files outside the repository
      via .. or symlink:
      - view_issue_raw_file()
        https://bugzilla.redhat.com/show_bug.cgi?id=2280726
      - generate_archive() CVE-2024-47515
      - _update_file_in_git()
        https://bugzilla.redhat.com/show_bug.cgi?id=2280723
    - Do not interpret filenames starting with - as git options
      in log() / view_history_file().
      https://bugzilla.redhat.com/show_bug.cgi?id=2315805
  * Drop / refresh patches.
  * Fix additional security issues:
    - Javascript prototype pollution (probably non-exploitable).
    - Quote non-escaping in HTML diffs.
  * Adapt to newer versions of dependencies:
    - Don't crash (many places).
    - Keep markdown alignment, keep reporting empty commits as empty.
    - Still possibly broken: plugins, dump and reload.
  * Tests:
    - Re-enable the build-time tests, using pytest.
    - Enable Salsa CI and autopkgtest, default to a subset for speed.
    - Don't crash when a test has no display name.
    - Accept changed error messages.
    - Skip code style checks.
    - Don't assume being in the source repo (e.g. find templates).
    - Clean up afterwards.
  * Javascript:
    - Minify with terser, copy if minification fails.
    - Actually install the minified version (and fix symlinks).
    - Switch back to Debian packaged libjs-jquery-atwho.
    - Add missing licenses to d/copyright.
  * d/watch: fix version duplication.
  * Fix spelling and grammar.
  * Bump Standards-Version to 4.7.0 (no changes needed).
  * Set Rules-Requires-Root: no.
  * New maintainer.  (Closes: #1073117)

 -- Rebecca N. Palmer <rebecca_palmer@zoho.com>  Tue, 14 Jan 2025 23:14:24 +0000

pagure (5.11.3+dfsg-4) unstable; urgency=medium

  * QA Upload

  [ Diane Trout ]
  * Update d/watch to as old watch pattern was missing a release
  * Add 0005-update-wtform-validator-call.patch (Closes: #1055204)
    Updates calls to wtforms.validators
  * Add 0006-use-markupsafe-escape.patch
    Deal with updated jinja2 library API

  [ Rebecca N. Palmer ]
  * Stop using removed cgi.escape (Closes: #1084587)
  * Fix SyntaxWarning. (Closes: #1085764)
  * Depend on celery not -common. (Closes: #1064530)
  * Clean up after documentation build. (Closes: #1046324)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 23 Dec 2024 20:19:04 +0100

pagure (5.11.3+dfsg-3) unstable; urgency=medium

  * d/control: Set Maintainer to Debian QA Group, orphaning package.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 12 Jun 2024 19:29:08 -0400

pagure (5.11.3+dfsg-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * No source change upload to move systemd units into /usr.

 -- Chris Hofstaedtler <zeha@debian.org>  Mon, 27 May 2024 01:11:31 +0200

pagure (5.11.3+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * d/pagure.postrm: Don't fail if adduser is not installed.
    (Closes: #1029626)

 -- Adrian Bunk <bunk@debian.org>  Tue, 21 Feb 2023 14:26:40 +0200

pagure (5.11.3+dfsg-2) unstable; urgency=medium

  [ Sergio Durigan Junior ]
  * d/control: Depend on git.
  * d/control: Depend on python3-email-validator, needed for pagure-admin.
  * d/README.Debian: Clarify that some Apache modules may need to be enabled.
    If the user chooses to use TLS, the she will need to enable the "ssl"
    and "headers" modules.
  * d/conf/pagure.conf: Fix Alias paths.
    The static files have been moved to /usr/share/pagure, so we need to
    update the paths.
  * d/control: Remove B-D on python3-nose{,xcover}. (Closes: #1018432)

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
    Changes-By: lintian-brush
    Fixes: lintian: upstream-metadata-file-is-missing
    See-also: https://lintian.debian.org/tags/upstream-metadata-file-is-missing.html
  * Update standards version to 4.5.1, no changes needed.
    Changes-By: lintian-brush
    Fixes: lintian: out-of-date-standards-version
    See-also: https://lintian.debian.org/tags/out-of-date-standards-version.html

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 22 Jan 2023 00:05:14 -0500

pagure (5.11.3+dfsg-1) unstable; urgency=medium

  * New upstream version 5.11.3+dfsg
  * Remove some local patches that are released by upstream.
    - d/p/0004-Fix-the-model-around-the-boards-so-it-works-with-mar.patch:
      Removed; released by upstream.
    - d/p/0005-Add-new-endpoints-to-the-API-documentation.patch: Likewise.
  * d/control: Add sqlite3 as B-D, needed for testing.
  * d/pagure-milters.tmpfile: Rename to ...tmpfiles.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Tue, 18 Aug 2020 22:51:33 -0400

pagure (5.11.2+dfsg-1) unstable; urgency=medium

  * d/NEWS: Fix typo.
  * d/control: Don't Depends/Recommends database servers.
    The user should not need to install a database server
    indiscriminately; she should be able to use an external DB if wanted.
  * d/README.Debian: Improve explanation about the use of DB servers.
  * d/copyright: Exclude *.min.js from vendor/dragula/.
    We're going to import the next release, which adds a new bundled JS
    library.
  * New upstream version 5.11.2+dfsg
  * d/control: B-D on python3-whitenoise.
  * Adjust flask_app.py to locate template, static and theme files.
    - d/p/0004-Adjust-flask_app.py-to-locate-template-static-and-th.patch:
      New patch.  On Debian, we install template, static and theme files
      under /usr/share/pagure (instead of
      /usr/lib/python3/dist-packages/pagure, which is upstream's default).
      For that reason, we have to adjust flask_app.py in order to make it
      properly locate these files.
  * Install templates, themes and static files under /usr/share/pagure.
    - d/pagure.install: Install templates, themes and static files under
      /usr/share/pagure.
    - d/pagure.links: Adjust links to /usr/share/pagure.
    - d/rules: Delete static, templates and themes after dh_install.
  * Fix alembic model for MariaDB/MySQL, and add new endpoints to the API
    documentation.
    - d/p/0004-Fix-the-model-around-the-boards-so-it-works-with-mar.patch:
      Fix alembic model.
    - d/p/0005-Add-new-endpoints-to-the-API-documentation.patch: Add new
      endpoints.
  * d/NEWS: Mention the database schema update.
  * d/control: Make pagure depend on alembic.
  * d/source/lintian-overrides: Add overrides for dragula.js.
    There are long lines in the files, but they are not minified.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 09 Aug 2020 22:47:06 -0400

pagure (5.10.0+dfsg-2) unstable; urgency=medium

  * Remove python3-funcsigs from B-D.
    This dependency is not needed anymore. (Closes: #959934)

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sat, 13 Jun 2020 12:47:11 -0400

pagure (5.10.0+dfsg-1) unstable; urgency=medium

  * Import jquery.atwho.
    Unfortunately, at.js is currently broken in Debian and preventing
    pagure from migrating to testing.  Since upstream ships the unminified
    version of package, we are going to use it instead.
  * New upstream version 5.10.0+dfsg
  * Remove unnecessary jquery.atwho links from d/links.
    We're not using the Debian package anymore.
  * Don't depend on libjs-jquery-atwho anymore.
    We are now using upstream's version of the package.
  * Add lintian overrides for jquery.atwho.
    There are big lines in the JS file, but it is not minified.
  * Delete sqlalchemy patch.
    Upstream has fixed the sqlalchemy problem.
  * Install pagure-authorized-keys-worker.service.
    Pagure now ships a specialized worker responsible for taking care of
    the ssh authentication, so now we have to ship it.
  * Add SSH_FOLDER to config and create $PAGURE_HOME/.ssh folder.
    The new Pagure authentication mechanism uses the $PAGURE_HOME/.ssh
    folder to store the authorized_keys file.
  * Drop the proxying to git-http-backend via the Apache configuration file.
    This reflects an upstream change due to the new support for http git
    push.
  * Ship new "plugins.cfg.sample" configuration file.
    Pagure now supports loading plugins, and so we have to ship the new
    sample configuration file to instruct the user how to do that.
  * Add NEWS file.
    We have to explain the new pagure-authorized-keys-worker service.
  * Expand section about the "pagure" auth backend on README.Debian.
    We now have to provide two solutions for the user: the previous way
    which involves modifying /etc/ssh/sshd_config, or the new
    'pagure-authorized-keys-worker' service.
  * B-D on python3-email-validator.
  * Bump debhelper-compat to 13.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Thu, 11 Jun 2020 13:30:11 -0400

pagure (5.9.1+dfsg-1) unstable; urgency=medium

  * New upstream version 5.9.1+dfsg.
  * Remove unnecessary step on d/rules to remove executable bits from files.
    It's not necessary to do this because we don't ship the problematic
    files anymore.  Moreover, the patch to fix this problem was accepted
    upstream.
  * Improve testing.
    It is necessary to set the PAGURE_CONFIG variable to point to a proper
    configuration file, so we do that.  I also decided to start compiling
    a list of testcases that do not properly run, and explicitly disable
    them.  Unfortunately the testsuite still fails sometimes, but the goal
    is to reach a state of 100% passes.
  * Remove duplicate build-depends on libjs-highlight.js.
  * Remove d/control.old.
    The file was included by mistake in the package.
  * Depend on libapache2-mod-wsgi-py3 instead of httpd-wsgi3.
    This reverts the decision I've made in the last release (see bug
    952708 for more details on it).  The reason I decided to depend
    directly on libapache2-mod-wsgi-py3 is because upstream does not
    officially support a non-Apache wsgi configuration.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 05 Apr 2020 20:14:08 -0400

pagure (5.8.1+dfsg-3) unstable; urgency=medium

  * Correct mistake and make sure we create HOME under /var/lib/pagure.
    In my last commit, I forgot to update the postinst files and make sure
    that the new HOME is /var/lib/pagure, and not /srv/pagure.
  * Add libjs-highlight.js as a dependency.
    This was a thinko; the dependency should have been added since the
    beginning.
  * Perform user/group replacement on service files on dh_installsystemd.
    There is no reason for us to keep various postinst scripts around just
    to perform the user/group adjustment on systemd service files when we
    can easily do that under the dh_installsystemd rule.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 04 Mar 2020 21:31:12 -0500

pagure (5.8.1+dfsg-2) unstable; urgency=medium

  * Mention pagure/utils.py in d/copyright.
  * Remove override_dh_installinit from d/rules.
  * Mark test B-D as "!nocheck".
    Also, reorganize a few dependencies, and add "python3-bs4" to the
    list.
  * Add Vcs-* fields.
  * Depend on httpd-wsgi3, instead of the Apache-specific package.
    Thanks to Michael Fladischer <fladi@debian.org> (Closes: #952708)
  * Depend on mariadb | postgres | sqlite3.
  * Enable the testsuite, but make it always pass, for now.
    The testsuite unfortunately contains several problems that need to be
    fixed before we are able to run it "for real".  Hopefully, these
    problems will all be addressed in the next release.
  * Use /var/lib/pagure instead of /srv/pagure as HOME.
    This is needed because Debian Policy explicitly says that /srv should
    be reserved to the user, and no package must install files there.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 04 Mar 2020 12:26:22 -0500

pagure (5.8.1+dfsg-1) unstable; urgency=medium

  * Initial release (Closes: #829046).

 -- Sergio Durigan Junior <sergiodj@debian.org>  Mon, 03 Feb 2020 18:49:20 -0500
